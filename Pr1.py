import math

def calc(a, b, action):
    try: 
        if action in ('+', '-', '*', '/', 'celi', 'floor', 'sin', 'cos'):
            if action == '+':
                print("%.2f" % (a+b))
            

            elif action == 'celi':
                print("%.2f" % math.celi(a))
            
            elif action == 'floor':
                print("%.2f" % math.floor(a))
            
            elif action == 'sin':
                print("%.2f" % math.sin(a))
            
            elif action == 'cos':
                print("%.2f" % math.cos(a))
            
            elif action == '-':
                print("%.2f" % (a-b))
            
            elif action == '*':
                print("%.2f" % (a*b))
            
            elif action == '%':
                print("%.2f" % (a%b))
            
            elif action == '//':
                print("%.2f" % (a//b))

            elif action == '**':
                print("%.2f" % (a**b))

            elif action == '/':
                if b != 0:
                    print("%.2f" % (a/b))
                else:
                    print("Бесконечность")
        
    except KeyError as e:
        raise ValueError('Неопределенная единица измерения: {}'.format(e.args[0]))


def stringInfo(s):
    print('Количество запятых в строке: ' , (s.count(',')))
    print('Количество пробелов в строке: ' , (s.count(' ')))
    print("Количество символов в строке: ", (len(s)))
    
    
def matx(col, row, start, step):
    mas = []
    x = start
    for i in range(col):
        mas.append([])
        for j in range(row):
            mas[i].append(x)
            x += step
    
    return mas


def stringInfoUI():
    s = input("Введите строку: ")
    stringInfo(s)


def calcUI():
    a = int(input("Введите a: ")) 
    action = input("Выберите действие (+, -, *, /, %, //, **, celi, floor, sin, cos)")
    if action in ('+', '-', '*', '/', '%', '//', '**'):
        b = int(input("Введите b: "))
    else:
        b = 0
    calc(a, b, action)


def matxUI():
    col = int(input("Ввод столбцов: "))
    row = int(input("Ввод строк: "))
    start = int(input("Введите начальный номер: "))
    step = int(input("Введите шаг: "))
    A = matx(col, row, start, step)
    for r in A:
        print(*r)


#main
def mainUI():
    while True:
        action = input("Выберите номер задания (1, 2, 3, exit)")
        if action == '1':
            calcUI()
        elif action == '2':
            stringInfoUI()
        elif action == '3':
            matxUI()
        elif action == 'exit':
            break


mainUI()












