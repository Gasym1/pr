def calc(a, b, action):
    try: 
        if action in ('+', '-', '*', '/', 'celi', 'floor', 'sin', 'cos'):
            if action == '+':
                print("%.2f" % (a+b))
            

            elif action == 'celi':
                print("%.2f" % math.celi(a))
            
            elif action == 'floor':
                print("%.2f" % math.floor(a))
            
            elif action == 'sin':
                print("%.2f" % math.sin(a))
            
            elif action == 'cos':
                print("%.2f" % math.cos(a))
            
            elif action == '-':
                print("%.2f" % (a-b))
            
            elif action == '*':
                print("%.2f" % (a*b))
            
            elif action == '%':
                print("%.2f" % (a%b))
            
            elif action == '//':
                print("%.2f" % (a//b))

            elif action == '**':
                print("%.2f" % (a**b))

            elif action == '/':
                if b != 0:
                    print("%.2f" % (a/b))
                else:
                    print("Бесконечность")
        
    except KeyError as e:
        raise ValueError('Неопределенная единица измерения: {}'.format(e.args[0]))